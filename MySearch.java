import java.util.*;
import java.lang.Math;

public class MySearch {
 
    
    public static int binarySearch(int[] inputArr, int key) {
         
        int start = 0;
        int end = inputArr.length - 1;
        while (start <= end) {
            int mid = (start + end) / 2;
            if (key == inputArr[mid]) {
                return mid;
            }
            if (key < inputArr[mid]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        return -1;
    }


    public static void mapSearch(int arr[], int n, int key){
        Map<Integer,Boolean> map= new HashMap<Integer,Boolean>();
        for(int i=0; i<n;i++){
            map.put(arr[i], true);
        }
        if(map.containsKey(key)){
            System.out.println("FOUND");
        }else{
            System.out.println("NOT FOUND");
        }

    /*    for(Integer val : map.keySet()){
            if(val == key){
                System.out.println(val);
            }
        
            }
    */            
    }


    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        int arr[]= new int[n];
        for(int j=0;j<n;j++){
            arr[j]=scan.nextInt();

        }
        System.out.println("Enter Key: ");
        int key= scan.nextInt();
        long startTimeB = System.currentTimeMillis();
        System.out.println("BINARY: "+ binarySearch(arr, key));
        long endTimeB = System.currentTimeMillis();
        long startTimeM = System.currentTimeMillis();       
        System.out.print("MAPSEARCH: ");
        mapSearch(arr, n, key);
        long endTimeM = System.currentTimeMillis();

        System.out.println(((long)endTimeB-startTimeB));
        System.out.println(((long)endTimeM-startTimeM));
        
    }
// if it prints -1, then data not found, else it returns the index of the data
//to compile: javac MySearch.java
//to run: java MySearch
// millisec: returns current time in milli-seconds in long
}



