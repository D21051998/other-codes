import java.util.LinkedList;
import java.util.Queue;

class Stack{
    Queue<Integer> q1 = new LinkedList<>();
    Queue<Integer> q2 = new LinkedList<>();

    void push(int x){
        q2.add(x);
        while(!q1.isEmpty()){
            q2.add(q1.remove());
        }
        Queue<Integer> q = new LinkedList<>();
        q = q1;
        q1 = q2;
        q2 = q;
    }

    void pop(){
        if(q1.isEmpty()) return;
        q1.remove();

    }
    int top(){
        return q1.peek();
    }

    int size(){
        return q1.size();
    }
}

public class QueuetoStack {

    public static void main(String args[]){

        Stack s = new Stack();
        s.push(5);
        System.out.println(s.top());
        s.push(10);
        s.pop();
        System.out.println(s.size());
        System.out.println(s.top());




    }

}
