#include<iostream>
using namespace std;
int swap(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
    cout<<"IN FUNCTION"<<"\n";
    cout<<a<<"\n";
    cout<<b<<"\n";
    return 0;
}

int main(){
    int a=10, b=20;
    swap(a,b);
    cout<<"AFTER FUNC"<<"\n";
    cout<<a<<"\n";
    cout<<b<<"\n";
    return 0;
}